package com.shiv.cloudcontractconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudcontractconsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudcontractconsumerApplication.class, args);
	}

}
